
Template.headerBar.onCreated(function() {

});

Template.headerBar.onRendered(function() {
    Session.set("mini", true);
    $('.collapsible').collapsible();
    $('.sidenav').sidenav();
    setTimeout(function() {
        $('.sidenav').sidenav();
        $(".dropdown-trigger").dropdown();
    }, 100)
    $(".dropdown-trigger").dropdown();
});

Template.headerBar.helpers({

});

Template.headerBar.events({
    'mouseover #sidebar-nav' (event) {
        let mini = Session.get("mini");
        if (mini) {
            console.log("opening sidebar");
            let sidenav = document.getElementById("sidebar-nav");
            sidenav.style.width = "250px";
            let mainId = document.getElementById("main")
            mainId.style.marginLeft = "250px";
            Session.set("mini", false);
        }
    },
    'mouseout #sidebar-nav' (event) {
        let mini = Session.get("mini");
        if (!mini) {
            console.log("closing sidebar");
            let sidenav = document.getElementById("sidebar-nav");
            sidenav.style.width = "40px";
            let mainId = document.getElementById("main")
            mainId.style.marginLeft = "40px";
            Session.set("mini", true);
        }
    },
	'click  .navBtn' (event) {
        event.preventDefault();
        var clickedTarget = event.target.id;
        console.log("clicked " + clickedTarget);
        if (clickedTarget == 'mainMenu') {
            FlowRouter.go('/');
        } else {
            console.log("should be going to /" + clickedTarget);
            FlowRouter.go('/' + clickedTarget);
        }
    },
    'click .signOut': () => {
        FlowRouter.go('/');
        Meteor.logout();
    },
    'click #brandLogo' (event) {
        event.preventDefault();
        FlowRouter.go('/dashboard');
    },
});
