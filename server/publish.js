import { EmailSettings } from '../imports/api/emailSettings.js';


Meteor.publish("emailSettings", function() {
    try {
        return EmailSettings.find({});
    } catch (error) {
        console.log("    ERROR pulling Email Settings data: " + error);
    }
});
