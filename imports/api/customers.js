import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Customers = new Mongo.Collection('customers');

Customers.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.customers' (customerName, customerPhone, customerPhoneType, customerAddress, customerAddressType, customerEmail, customerEmailType) {

    },
    'edit.customers' (customerId, customerName, customerPhone, customerPhoneType, customerAddress, customerAddressType, customerEmail, customerEmailType) {

    },
    'delete.customers' (customerId) {
        
    }
});