import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const EmailSettings = new Mongo.Collection('emailSettings');

EmailSettings.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.emailSettings' (smtpHost, smtpPort, smtpUser, smtpPassword, isTLS) {

    },
    'edit.emailSettings' (emailSettingId, smtpHost, smtpPort, smtpUser, smtpPassword, isTLS) {

    },
    'delete.emailSettings' (emailSettingId) {
        
    }
});